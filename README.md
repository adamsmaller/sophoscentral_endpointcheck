# SophosCentral_EndpointCheck

## Purpose

I created this script to monitor the status of Sophos Endpoint on Windows servers by querying the Sophos Central API after an RMM monitoring script failed to run this check on the client-side.

## Steps

1. Obtain API credentials using the guide [here](https://doc.sophos.com/central/partner/help/en-us/Help/SettingsAndPolicies/APICredentials/index.html)
2. On line 9, set a value for `<client_id>` and `<client_secret>` 
3. On line 12, set the value `<partner_id>`
4. If required, create a file to store tenant IDs to be ignored and store it in the same dir as the script (see line 27 - `tenant_ids.txt`)
