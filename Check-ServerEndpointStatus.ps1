$ErrorActionPreference = "SilentlyContinue"

# Authentication

$url = "https://id.sophos.com/api/v2/oauth2/token"
$headers = @{
    "Content-Type" = "application/x-www-form-urlencoded"
}
$body = "grant_type=client_credentials&client_id=<client_id>&client_secret=<client_secret>&scope=token"
$response = Invoke-RestMethod -Uri $url -Method POST -Headers $headers -Body $body
$accessToken = $response.access_token
$partnerID = # <partner_id>

# Generate tenant page list

$pages = @()
$x = 1
while ($x -lt 8) {
    # Write-Host -ForegroundColor Green "Getting page URLs..."
    $url = "https://api.central.sophos.com/partner/v1/tenants?page=$x" # gets 50 resutls by default | 7 pages in total
    $pages += $url
    $x = $x + 1
}

# Generate list of tenant IDs to ignore
$ignoredTenants = @()
foreach ($tenantId in $(Get-Content -Path ".\tenant_ids.txt")) {
    $ignoredTenants += $tenantId
}

# Main process
foreach ($url in $pages) {
    $headers = @{
        "Authorization" = "Bearer $accessToken"
        "X-Partner-ID" = $partnerID
    }
    $response = Invoke-RestMethod -Uri $url -Method GET -Headers $headers # tenants stored in $response.items
    foreach ($tenant in $response.items) {
        if ($tenant.id -notin $ignoredTenants -and $tenant.name -notmatch "<company_name>") {
            Write-Host -ForegroundColor Green "Checking endpoints on $($tenant.name)'s tenant."
            $url = "$($tenant.apiHost)/endpoint/v1/endpoints"
            $headers = @{
                "Authorization" = "Bearer $accessToken"
                "X-Tenant-ID" = $tenant.id
            }
            $endpoints = Invoke-RestMethod -Uri $url -Method GET -Headers $headers
            $servers = $endpoints.items | Where-Object {$_.type -eq "server" -and $_.assignedProducts -ne $null}
            foreach ($server in $servers) {
                if ($server.health.overall -ne "good" -and $($server.lastSeenAt) -gt $(Get-Date).AddDays(-60)) {
                    if ($server.health.threats.status -ne "good") {
                        # Write-Host -ForegroundColor Red "Sophos Endpoint on $($tenant.showAs)'s '$($server.hostname)' server (last seen on $($server.lastSeenAt)) has identified suspicious files."
                        Send-MailMessage -SmtpServer "<server_addr>" -From "<sender_email>" `
                        -To "<recipient_addr>" -Subject "$($tenant.name) - Sophos Endpoint - Degraded health" `
                        -Body "Sophos Endpoint on $($tenant.showAs)'s '$($server.hostname)' server has identified suspicious files." -UseSsl -Port 25
                    }
                    elseif ($server.health.services.status -ne "good") {
                        # Write-Host -ForegroundColor Red "Sophos Endpoint on $($tenant.showAs)'s '$($server.hostname)' server (last seen on $($server.lastSeenAt)) has a degraded service/component."
                        Send-MailMessage -SmtpServer "<server_addr>" -From "<sender_email>" `
                        -To "<recipient_addr>" -Subject "$($tenant.name) - Sophos Endpoint - Degraded health" `
                        -Body "Sophos Endpoint on $($tenant.showAs)'s '$($server.hostname)' server has a degraded service/component." -UseSsl -Port 25
                    }
                    elseif ($server.health.threats.status -eq "good" -and $server.health.services.status -eq "good") {
                        # Write-Host -ForegroundColor Red "Sophos Endpoint on $($tenant.showAs)'s '$($server.hostname)' server (last seen on $($server.lastSeenAt)) may be outdated."
                        Send-MailMessage -SmtpServer "<server_addr>" -From "<sender_email>" `
                        -To "<recipient_addr>" -Subject "$($tenant.name) - Sophos Endpoint - Degraded health" `
                        -Body "Sophos Endpoint on $($tenant.showAs)'s '$($server.hostname)' server may be outdated." -UseSsl -Port 25
                    }
                }
            }
        }
    }
}
